
const path = require('path')

module.exports = {
  mode: 'development',
  entry: './precompiled/index.ts',
  output: {
    filename: 'index.js',
    libraryTarget: 'umd',
    path: path.resolve(__dirname, 'compiled')
  },
  module: {
    rules: [{
      test: /\.tsx?$/,
      use: 'ts-loader',
      exclude: /node_modules/
    }]
  },
  resolve: {
    extensions: [ '.tsx', '.ts', '.js' ],
    alias: {
      'precompiled': path.resolve(__dirname, './precompiled'),
      '@': path.resolve(__dirname, './precompiled')
    }
  },
  devtool: false
}

