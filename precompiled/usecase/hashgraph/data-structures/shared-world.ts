
import { HashgraphShard, HashgraphShardMember } from './shard'
import { HashgraphEvent } from './event'

import { createShard } from '../algorithms/shard/create-shard'
import { manualSync } from '../algorithms/shard/manual-sync'
import { startAutoSync } from '../algorithms/shard/start-auto-sync'
import { stopAutoSync } from '../algorithms/shard/stop-auto-sync'
import { setSyncTickRate } from '../algorithms/shard/set-sync-tick-rate'
import { onSendGossip } from '../algorithms/shard/on-send-gossip'
import { sendGossip } from '../algorithms/shard/send-gossip'
import { receiveGossip } from '../algorithms/shard/receive-gossip'
import { onReceiveGossip } from '../algorithms/shard/on-receive-gossip'
import { onAddMember } from '../algorithms/shard/on-add-member'
import { setElapsedTimeToDeleteEventHistory } from '../algorithms/shard/set-elapsed-time-to-delete-event-history'
import { addMember } from '../algorithms/shard/add-member'
import { removeMember } from '../algorithms/shard/remove-member'
import { setMinimumVotePercentToAddMember } from '../algorithms/shard/set-minimum-vote-percent-to-add-member'
import { setMinimumVotePercentToRemoveMember } from '../algorithms/shard/set-minimum-vote-percent-to-remove-member'
import { onMemberUpdate } from '../algorithms/shard/on-member-update'
import { getMember } from '../algorithms/shard/get-member'
import { getMemberPublicKey } from '../algorithms/shard/get-member-public-key'
import { getMemberList } from '../algorithms/shard/get-member-list'
import { getEvent } from '../algorithms/shard/get-event'
import { togglePeerConnection } from '../algorithms/shard/toggle-peer-connection'
import { onTransactionOrder } from '../algorithms/shard/on-transaction-order'
import { sendTransaction } from '../algorithms/shard/send-transaction'
import { determineTransactionOrder } from '../algorithms/shard/determine-transaction-order'
import { getState } from '../algorithms/shard/get-state'




// Create SharedWorld instance to interact with a hashgraph shard.
class SharedWorld {
  shard: HashgraphShard

  constructor () {
    this.shard = createShard()
  }

  manualSync (targetPeerID?: string, extract?: any): Promise<void> {
    return manualSync(this.shard, targetPeerID, extract)
  }

  startAutoSync (extract?: any): void {
    startAutoSync(this.shard, extract)
  }

  stopAutoSync (extract?: any): void {
    stopAutoSync(this.shard, extract)
  }

  setSyncTickRate (tickRate: number, extract?: any): void {
    setSyncTickRate(this.shard, tickRate, extract)
  }

  onSendGossip (callback: (targetPeerID: string, eventList: HashgraphEvent[]) => Promise<void>|void, extract?: any): void {
    onSendGossip(this.shard, callback, extract)
  }

  sendGossip (targetPeerID?: string, extract?: any): Promise<void> {
    return sendGossip(this.shard, targetPeerID, extract)
  }

  receiveGossip (eventList: HashgraphEvent[], extract?: any): Promise<void> {
    return receiveGossip(this.shard, eventList, extract)
  }

  onReceiveGossip (callback: (eventList: HashgraphEvent[]) => void, extract?: any): void {
    onReceiveGossip(this.shard, callback, extract)
  }

  onAddMember (callback: (candidateMember: HashgraphShardMember) => Promise<boolean>|boolean, extract?: any): void {
    onAddMember(this.shard, callback, extract)
  }

  setElapsedTimeToDeleteEventHistory (elapsedTime: number, extract?: any): void {
    setElapsedTimeToDeleteEventHistory(this.shard, elapsedTime, extract)
  }

  addMember (peerID?: string, isSelf?: boolean, isConnectedPeer?: boolean, candidateMember?: HashgraphShardMember, skipVoting?: boolean, extract?: any): Promise<string> {
    return addMember(this.shard, peerID, isSelf, isConnectedPeer, candidateMember, skipVoting, extract)
  }

  removeMember (candidateMember: HashgraphShardMember, skipVoting?: boolean, extract?: any): void {
    return removeMember(this.shard, candidateMember, skipVoting, extract)
  }

  setMinimumVotePercentToAddMember (callback: (candidateMember: HashgraphShardMember) => Promise<number>|number, extract?: any): void {
    return setMinimumVotePercentToAddMember(this.shard, callback, extract)
  }

  setMinimumVotePercentToRemoveMember (callback: (candidateMember: HashgraphShardMember) => Promise<number>|number, extract?: any): void {
    return setMinimumVotePercentToRemoveMember(this.shard, callback, extract)
  }

  onMemberUpdate (callback: (member: HashgraphShardMember, updateType: string) => void, extract?: any): void {
    onMemberUpdate(this.shard, callback, extract)
  }

  getMember (peerID: string, extract?: any): HashgraphShardMember {
    return getMember(this.shard, peerID, extract)
  }

  getMemberPublicKey (peerID: string, extract?: any): string {
    return getMemberPublicKey(this.shard, peerID, extract)
  }

  getMemberList (extract?: any): HashgraphShardMember[] {
    return getMemberList(this.shard, extract)
  }

  getEvent (eventID: string, extract?: any): HashgraphEvent {
    return getEvent(this.shard, eventID, extract)
  }

  togglePeerConnection (peerID: string, isConnectedPeer?: boolean, extract?: any): void {
    togglePeerConnection(this.shard, peerID, isConnectedPeer, extract)
  }

  onTransactionOrder (callback: (previousState: any, transactionList: any[], isConsensusOrder: boolean) => Promise<void>|void, extract?: any): void {
    onTransactionOrder(this.shard, callback, extract)
  }

  sendTransaction (transaction: any, extract?: any): void {
    sendTransaction(this.shard, transaction, extract)
  }

  determineTransactionOrder (extract?: any): Promise<void> {
    return determineTransactionOrder(this.shard, extract)
  }

  getState (extract?: any): any {
    return getState(this.shard, extract)
  }
}

export {
  SharedWorld
}