
class HashgraphEvent {
  uid: string = ''
  payload: any = ''
  selfParentHash: string = ''
  otherParentHash: string = ''
  timestamp: string = ''
  creatorUID: string = ''
  signature: string = ''

  orderIndex: number = -1

  roundCreated: number = -1
  roundReceived: number = -1
  isWitness: boolean = false
  isFamous: boolean = false
  isFameDecided: boolean = false
  consensusTimestamp: string = ''

  constructor (payload: string, creatorUID: string, selfParentHash?: string, otherParentHash?: string) {
    this.timestamp = Date.now().toString()

    this.payload = payload
    this.creatorUID = creatorUID
    this.selfParentHash = selfParentHash || ''
    this.otherParentHash = otherParentHash || ''
  }
}

export {
  HashgraphEvent
}
