
import { HashgraphMember } from './member'

import { HashgraphEvent } from './event'

class Hashgraph {
  uid: string = ''

  nPopulationSize: number = 0
  cFrequencyOfCoinRounds: number = 10
  dRoundsDelayed: number = 1

  memberTable: { [uid: string]: HashgraphMember } = {}

  eventTable: { [uid: string]: HashgraphEvent } = {}

  eventRoundCreatedTable: { [round: number]: {
    memberTable: { [memberUID: string]: boolean }
    eventTable: { [eventUID: string ]: boolean }
    witnessEventTable: { [eventUID: string]: boolean }
    famousEventTable: { [eventUID: string]: boolean }
    notFamousEventTable: { [eventUID: string]: boolean }
  }} = {}

  eventRoundReceivedTable: { [round: number]: {
    eventTable: { [eventUID: string]: {
      medianTimestamp: string
      timestampList: string[]
    }}
  }} = {}

  latestRoundCreated: number = 1
  latestRoundReceived: number = 1
  earliestRoundCreatedWithKnownConsensus: number = 0
}

interface VoteTable {
  [eventCandidateUID: string]: {
    numOfUniqueMemberVoteYes: number
    numOfUniqueMemberVoteNo: number
    numOfUniqueMemberVoters: number
    voteRecord: { [memberUID: string]: {
      hasAnEventVotedYes: boolean
      hasAnEventVotedNo: boolean
      eventTable: { [eventVoterUID: string]: { vote: boolean, hasVoted: boolean } }
    }}
    activeMemberTable: { [memberUID: string]: any }
  }
}

interface UnknownEventTable {
  [uid: string]: {
    isAddedToList: boolean
    callbackList: Array<() => void>
  }
}

interface EventPathTable {
  [eventUID: string]: {
    hasTargetAsAncestor: boolean
    isAncestorRelationKnown: boolean
    childEventList: string[]
  }
}

export {
  Hashgraph,
  VoteTable,
  UnknownEventTable,
  EventPathTable
}
