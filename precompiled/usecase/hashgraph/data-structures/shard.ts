import {
  Hashgraph
} from './hashgraph'

import {
  HashgraphEvent
} from './event'

class HashgraphShard {
  hashgraph: Hashgraph

  selfPeerID: string = ''
  peerIDPublicKeyMap: { [peerID: string]: string } = {}
  publicKeyPeerIDMap: { [publicKey: string]: string } = {}
  connectedPeerMap: { [peerID: string]: boolean } = {}

  addMemberCandidateTable: { [candidateUID: string]: {
    candidate: HashgraphShardMember, roundNominated: number, voteTable: { [voterUID: string]: boolean }, minimumVotePercent: number
  } } = {}
  removeMemberCandidateTable: { [candidateUID: string]: {
    candidate: HashgraphShardMember, roundNominated: number, voteTable: { [voterUID: string]: boolean }, minimumVotePercent: number
  } } = {}

  defaultMinimumVotePercent: number = 66 // default to at least 2/3 of current population to add or remove members

  transactionQueue: any[] = []

  latestRoundReceived: number = -1
  stateTable: { [roundReceived: number]: any } = {}

  syncInterval?: NodeJS.Timer|number
  syncTickRate: number = 1000 // default to 1000 milliseconds per syncs
  isStartedAutoSync: boolean = false

  deleteEventHistoryInterval?: NodeJS.Timer|number
  elapsedTimeToDeleteEventHistory: number = 20 // default to 20 seconds to delete history
  isStartedAutoDelete: boolean = false

  onSendGossipCallback?: (targetPeerID: string, eventList: HashgraphEvent[]) => Promise<void>|void
  onReceiveGossipCallback?: (eventList: HashgraphEvent[]) => void
  onAddMemberCallback?: (candidateMember: HashgraphShardMember) => Promise<boolean>|boolean
  setVotePercentToAddMemberCallback?: (candidateMember: HashgraphShardMember) => Promise<number>|number
  onRemoveMemberCallback?: (candidateMember: HashgraphShardMember) => Promise<boolean>|boolean
  setVotePercentToRemoveMemberCallback?: (candidateMember: HashgraphShardMember) => Promise<number>|number
  onMemberUpdateCallback?: (member: HashgraphShardMember, updateType: string) => void
  onTransactionOrderCallback?: (previousState: any, transactionList: any[], isConsensusOrder: boolean) => Promise<void>|void

  constructor (hashgraph: Hashgraph) {
    this.hashgraph = hashgraph
  }
}

interface HashgraphShardMember {
  publicKey: string
  peerID?: string
  peerConnectionMap?: { [publicKey: string]: boolean }
  details?: any
}

const HASHGRAPH_SHARD_ERROR_PREFIX: string = 'Hashgraph Shard Error:'

const HASHGRAPH_SHARD_MESSAGE_TYPE: string = 'HashgraphShardMessageType'
const HASHGRAPH_SHARD_ADD_MEMBER_NOMINATION: string = 'HASHGRAPH_SHARD_ADD_MEMBER_NOMINATION'
const HASHGRAPH_SHARD_REMOVE_MEMBER_NOMINATION: string = 'HASHGRAPH_SHARD_REMOVE_MEMBER_NOMINATION'
const HASHGRAPH_SHARD_ADD_MEMBER_VOTE: string = 'HASHGRAPH_SHARD_ADD_MEMBER_VOTE'
const HASHGRAPH_SHARD_REMOVE_MEMBER_VOTE: string = 'HASHGRAPH_SHARD_REMOVE_MEMBER_VOTE'

const MEMBER_UPDATE_TYPE_ADDED: string = 'added'
const MEMBER_UPDATE_TYPE_REMOVED: string = 'removed'


export {
  HashgraphShard,
  HashgraphShardMember,
  HASHGRAPH_SHARD_ERROR_PREFIX,
  HASHGRAPH_SHARD_MESSAGE_TYPE,
  HASHGRAPH_SHARD_ADD_MEMBER_NOMINATION,
  HASHGRAPH_SHARD_REMOVE_MEMBER_NOMINATION,
  HASHGRAPH_SHARD_ADD_MEMBER_VOTE,
  HASHGRAPH_SHARD_REMOVE_MEMBER_VOTE,
  MEMBER_UPDATE_TYPE_ADDED,
  MEMBER_UPDATE_TYPE_REMOVED
}
