
class HashgraphMember {
  uid: string = ''
  publicKey: string = ''
  privateKey: string = ''

  orderedEventUidList: string[] = []
  unorderedEventUidList: string[] = []

  roundEnteredList: number[] = []
  roundExitedList: number[] = []
}

export {
  HashgraphMember
}
