import { Hashgraph, UnknownEventTable } from '../../data-structures/hashgraph'
import { HashgraphMember } from '../../data-structures/member'
import { HashgraphEvent } from '../../data-structures/event'

import { getMemberLatestEvent } from './get-member-latest-event'

const MAX_NUM_EVENTS_SEARCHED: number = 100


function getUnknownEventListForMember (hashgraph: Hashgraph, member: HashgraphMember, extract?: any): HashgraphEvent[] {
  const eventQueue: string[] = []
  let numEventsSearched: number = 0

  const isVisitedEventTable: { [eventUID: string]: boolean } = {}
  const latestAncestorByMember: { [memberUID: string]: { eventUID: string, orderIndex: number } } = {}
  let numMembersVisited: number = 0

  const latestEvent: HashgraphEvent|null = getMemberLatestEvent(hashgraph, member, extract)

  if (latestEvent) {
    latestAncestorByMember[latestEvent.creatorUID] = { eventUID: latestEvent.uid, orderIndex: hashgraph.eventTable[latestEvent.uid].orderIndex }
    eventQueue.unshift(latestEvent.selfParentHash)
    eventQueue.unshift(latestEvent.otherParentHash)
  }

  // Search for the latest ancestor events for each member
  while (eventQueue.length > 0 && numEventsSearched < MAX_NUM_EVENTS_SEARCHED) {
    const hashgraphEventUID = eventQueue.pop()
    if (!hashgraphEventUID || isVisitedEventTable[hashgraphEventUID]) { continue }

    isVisitedEventTable[hashgraphEventUID] = true
    const hashgraphEvent = hashgraph.eventTable[hashgraphEventUID]

    if (!hashgraphEvent) { continue }

    if (latestAncestorByMember[hashgraphEvent.creatorUID]) {
      if (latestAncestorByMember[hashgraphEvent.creatorUID].orderIndex < hashgraphEvent.orderIndex) {
        latestAncestorByMember[hashgraphEvent.creatorUID].eventUID = hashgraphEventUID
        latestAncestorByMember[hashgraphEvent.creatorUID].orderIndex = hashgraphEvent.orderIndex
      }
    } else {
      latestAncestorByMember[hashgraphEvent.creatorUID] = { eventUID: hashgraphEventUID, orderIndex: hashgraphEvent.orderIndex }
      numMembersVisited++
    }

    if (numMembersVisited === hashgraph.nPopulationSize) {
      break
    }

    if (hashgraphEvent.selfParentHash) {
      eventQueue.unshift(hashgraphEvent.selfParentHash)
    }

    if (hashgraphEvent.otherParentHash) {
      eventQueue.unshift(hashgraphEvent.otherParentHash)
    }

    numEventsSearched++
  }

  // Record the descendants of the latest ancestor events for each member
  let descendantEventList: HashgraphEvent[] = []
  const unknownEventTable: UnknownEventTable = {}

  for (const memberUID of Object.keys(hashgraph.memberTable)) {

    let latestAncestorEventOrderIndex = -1
    if (latestAncestorByMember[memberUID]) {
      latestAncestorEventOrderIndex = latestAncestorByMember[memberUID].orderIndex
    }

    const hashgraphMember = hashgraph.memberTable[memberUID]

    // Ensure we collect the latest ancestor event from all members. Track the earliest event of a member if the
    // member has no events that are ancestors to the target member's latest event.
    const selfDescendantEventUidList = hashgraphMember.orderedEventUidList.slice(latestAncestorEventOrderIndex + 1) || []

    const selfDescendantEventList = selfDescendantEventUidList.map((uid) => hashgraph.eventTable[uid]).filter((event) => event ? true : false)

    for (const event of selfDescendantEventList) {
      unknownEventTable[event.uid] = { isAddedToList: false, callbackList: [] }
    }

    descendantEventList = descendantEventList.concat(selfDescendantEventList)
  }

  // Sort the descendant events by the earliest parents first (topological order)

  const unknownEventList: HashgraphEvent[] = []

  for (const event of descendantEventList) {
    let hasAddedSelfParent = true
    let hasAddedOtherParent = true

    if (unknownEventTable[event.selfParentHash]) {
      hasAddedSelfParent = unknownEventTable[event.selfParentHash].isAddedToList
    }

    if (unknownEventTable[event.otherParentHash]) {
      hasAddedOtherParent = unknownEventTable[event.otherParentHash].isAddedToList
    }

    if (!(hasAddedSelfParent && hasAddedOtherParent)) {
      if (!hasAddedSelfParent) {
        unknownEventTable[event.selfParentHash].callbackList.push(() => { addEventToUnknownEventList(unknownEventTable, unknownEventList, event, extract) })
      }

      if (!hasAddedOtherParent) {
        unknownEventTable[event.otherParentHash].callbackList.push(() => { addEventToUnknownEventList(unknownEventTable, unknownEventList, event, extract) })
      }

      continue
    }

    addEventToUnknownEventList(unknownEventTable, unknownEventList, event, extract)
  }

  return unknownEventList
}

function addEventToUnknownEventList (unknownEventTable: UnknownEventTable, unknownEventList: HashgraphEvent[], event: HashgraphEvent, _extract?: any): void {
  let hasAddedSelfParent = true
  let hasAddedOtherParent = true

  if (unknownEventTable[event.selfParentHash]) {
    hasAddedSelfParent = unknownEventTable[event.selfParentHash].isAddedToList
  }

  if (unknownEventTable[event.otherParentHash]) {
    hasAddedOtherParent = unknownEventTable[event.otherParentHash].isAddedToList
  }

  if (unknownEventTable[event.uid].isAddedToList || !(hasAddedSelfParent && hasAddedOtherParent)) {
    return
  }

  unknownEventList.push(event)
  unknownEventTable[event.uid].isAddedToList = true

  for (const callback of unknownEventTable[event.uid].callbackList) {
    callback()
  }
}

export {
  getUnknownEventListForMember
}