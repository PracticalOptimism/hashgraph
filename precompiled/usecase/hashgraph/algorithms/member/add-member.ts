import { Hashgraph } from '../../data-structures/hashgraph'
import { HashgraphMember } from '../../data-structures/member'


function addMember (hashgraph: Hashgraph, member: HashgraphMember, roundAdded?: number, _extract?: any): void {

  const latestRoundEntered = member.roundEnteredList[member.roundEnteredList.length - 1] || -1
  const latestRoundExitedForEnteredRound = member.roundExitedList[member.roundEnteredList.length - 1] || -1

  // Ensure member hasn't already been added.
  if (latestRoundEntered > 0 && latestRoundExitedForEnteredRound < 0) { return }

  member.roundEnteredList.push(roundAdded || hashgraph.latestRoundCreated)

  hashgraph.memberTable[member.uid] = member
  hashgraph.nPopulationSize = Object.keys(hashgraph.memberTable).length
}

export {
  addMember
}