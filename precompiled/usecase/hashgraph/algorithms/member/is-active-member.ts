import { Hashgraph } from '../../data-structures/hashgraph'

function isActiveMember (hashgraph: Hashgraph, uid: string, round?: number, _extract?: any): boolean {
  const member = hashgraph.memberTable[uid]

  if (!member) { return false }

  if (!round) {
    round = hashgraph.latestRoundCreated
  }

  for (let i = 0; i < member.roundEnteredList.length; i++) {
    const roundEntered = member.roundEnteredList[i] || 0
    const roundExited = member.roundExitedList[i] || Infinity

    if (roundEntered > round || roundExited < round) { continue }

    return true
  }

  return false
}

export {
  isActiveMember
}