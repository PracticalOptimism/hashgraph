import { Hashgraph } from '../../data-structures/hashgraph'

function removeMember (hashgraph: Hashgraph, uid: string, roundRemoved?: number, _extract?: any): void {
  const member = hashgraph.memberTable[uid]

  if (!member) { return }

  const latestRoundEntered = member.roundEnteredList[member.roundEnteredList.length - 1] || -1
  const latestRoundExitedForEnteredRound = member.roundExitedList[member.roundEnteredList.length - 1] || -1

  // Ensure member hasn't already been removed.
  if (latestRoundEntered > 0 && latestRoundExitedForEnteredRound > 0) { return }

  member.roundExitedList.push(roundRemoved || hashgraph.latestRoundCreated)
}

export {
  removeMember
}