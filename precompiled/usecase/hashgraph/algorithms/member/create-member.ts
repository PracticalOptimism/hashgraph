import { HashgraphMember } from '../../data-structures/member'

import { extractProvider } from '../../../../provider/extract'

async function createMember (publicKey?: string, extract?: any): Promise<HashgraphMember> {
  const hashgraphMember = new HashgraphMember()

  if (!publicKey) {
    const { cryptography } = extractProvider(extract)
    const keys = (await cryptography.generateKeyPair())[0]
    hashgraphMember.privateKey = keys.privateKey
    hashgraphMember.publicKey = keys.publicKey
    hashgraphMember.uid = keys.publicKey
  } else {
    hashgraphMember.publicKey = publicKey
    hashgraphMember.uid = publicKey
  }

  return hashgraphMember
}

export {
  createMember
}