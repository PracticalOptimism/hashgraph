import { Hashgraph } from '../../data-structures/hashgraph'
import { HashgraphMember } from '../../data-structures/member'

function getMemberList (hashgraph: Hashgraph, round?: number, _extract?: any): HashgraphMember[] {
  const memberList: HashgraphMember[] = []

  round = round || hashgraph.latestRoundCreated

  for (const memberUID of Object.keys(hashgraph.memberTable)) {
    const member = hashgraph.memberTable[memberUID]

    for (let i = 0; i < member.roundEnteredList.length; i++) {
      const roundEntered = member.roundEnteredList[i] || 0
      const roundExited = member.roundExitedList[i] || Infinity

      if (roundEntered > round || roundExited < round) { continue }

      memberList.push(member)
      break
    }
  }

  return memberList
}

export {
  getMemberList
}