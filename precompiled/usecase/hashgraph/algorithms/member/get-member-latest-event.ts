import { Hashgraph } from '../../data-structures/hashgraph'
import { HashgraphMember } from '../../data-structures/member'
import { HashgraphEvent } from '../../data-structures/event'

function getMemberLatestEvent (hashgraph: Hashgraph, member: HashgraphMember, _extract?: any): HashgraphEvent|null {
  if (!member) { return null }

  if (member.orderedEventUidList.length === 0) { return null }

  const latestEventUID = member.orderedEventUidList[member.orderedEventUidList.length - 1]
  return hashgraph.eventTable[latestEventUID] || null
}

export {
  getMemberLatestEvent
}