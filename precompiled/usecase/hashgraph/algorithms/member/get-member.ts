import { Hashgraph } from '../../data-structures/hashgraph'
import { HashgraphMember } from '../../data-structures/member'

function getMember (hashgraph: Hashgraph, uid: string, _extract?: any): HashgraphMember {
  return hashgraph.memberTable[uid]
}

export {
  getMember
}