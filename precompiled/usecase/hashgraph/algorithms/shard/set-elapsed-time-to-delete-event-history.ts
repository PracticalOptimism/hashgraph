
import { HashgraphShard } from '../../data-structures/shard'

import { startAutoDeleteEventHistory } from './start-auto-delete-event-history'

function setElapsedTimeToDeleteEventHistory (shard: HashgraphShard, elapsedTime: number, extract?: any): void {
  if (elapsedTime <= 0) {
    elapsedTime = 60
  }
  shard.elapsedTimeToDeleteEventHistory = elapsedTime

  // Reset the auto delete frequency
  if (shard.isStartedAutoDelete) {
    startAutoDeleteEventHistory(shard, extract)
  }
}

export {
  setElapsedTimeToDeleteEventHistory
}