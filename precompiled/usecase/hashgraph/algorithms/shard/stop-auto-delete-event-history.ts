import { HashgraphShard } from '../../data-structures/shard'

function stopAutoDeleteEventHistory (shard: HashgraphShard, _extract?: any): void {
  if (!shard.deleteEventHistoryInterval) {
    return
  }
  clearInterval(shard.deleteEventHistoryInterval as NodeJS.Timer)
  shard.isStartedAutoDelete = false
}

export {
  stopAutoDeleteEventHistory
}