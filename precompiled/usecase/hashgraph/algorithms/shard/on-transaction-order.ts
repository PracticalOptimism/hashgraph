
import { HashgraphShard } from '../../data-structures/shard'

function onTransactionOrder (shard: HashgraphShard, callback: (previousState: any, transactionList: any[], isConsensusOrder: boolean) => Promise<void>|void, _extract?: any): void {
  shard.onTransactionOrderCallback = callback
}

export {
  onTransactionOrder
}