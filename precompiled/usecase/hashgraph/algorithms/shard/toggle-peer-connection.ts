
import {
  HashgraphShard,
  HASHGRAPH_SHARD_ERROR_PREFIX
} from '../../data-structures/shard'

function togglePeerConnection (shard: HashgraphShard, peerID: string, isConnectedPeer?: boolean, _extract?: any): void {
  const memberPublicKey = shard.peerIDPublicKeyMap[peerID]
  if (!memberPublicKey) {
    console.warn(`${HASHGRAPH_SHARD_ERROR_PREFIX} Cannot connect to peer that is not already a member of hashgraph with uid: ${shard.hashgraph.uid}`)
    return
  }

  if (isConnectedPeer) {
    shard.connectedPeerMap[peerID] = true
  } else {
    delete shard.connectedPeerMap[peerID]
  }
}

export {
  togglePeerConnection
}