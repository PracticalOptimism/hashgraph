
import { HashgraphShard, HashgraphShardMember } from '../../data-structures/shard'

import { isActiveMember } from '../member/is-active-member'
import { getMemberPublicKey } from './get-member-public-key'
import { sendTransaction } from './send-transaction'

import {
  HASHGRAPH_SHARD_MESSAGE_TYPE,
  HASHGRAPH_SHARD_ADD_MEMBER_VOTE
} from '../../data-structures/shard'


async function onAddMemberNomination (shard: HashgraphShard, candidateMember: HashgraphShardMember, roundNominated: number, extract?: any): Promise<void> {

  // Ensure candidate isn't already active.
  if (isActiveMember(shard.hashgraph, candidateMember.publicKey, extract)) { return }

  // Ensure member hasn't already been nominated.
  if (shard.addMemberCandidateTable[candidateMember.publicKey]) { return }

  let minimumVotePercent: number = shard.defaultMinimumVotePercent
  if (shard.setVotePercentToAddMemberCallback) {
    minimumVotePercent = await shard.setVotePercentToAddMemberCallback(candidateMember)
  }

  // Add the member as a candidate
  shard.addMemberCandidateTable[candidateMember.publicKey] = {
    candidate: candidateMember, roundNominated, voteTable: {}, minimumVotePercent
  }

  const selfMemberPublicKey: string = getMemberPublicKey(shard, shard.selfPeerID, extract)

  if (!selfMemberPublicKey) { return }

  let selfMemberVote: boolean = true
  if (shard.onAddMemberCallback) {
    selfMemberVote = await shard.onAddMemberCallback(candidateMember)
  }

  // Send a vote
  sendTransaction(shard, {
    [HASHGRAPH_SHARD_MESSAGE_TYPE]: HASHGRAPH_SHARD_ADD_MEMBER_VOTE,
    candidateMember, votingMember: { publicKey: selfMemberPublicKey }, vote: selfMemberVote
  }, extract)
}

export {
  onAddMemberNomination
}