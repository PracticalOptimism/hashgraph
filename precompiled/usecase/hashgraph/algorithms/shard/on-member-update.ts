import { HashgraphShard, HashgraphShardMember } from '../../data-structures/shard'

function onMemberUpdate (shard: HashgraphShard, callback: (member: HashgraphShardMember, updateType: string) => void, _extract?: any): void {
  shard.onMemberUpdateCallback = callback
}

export {
  onMemberUpdate
}