import { HashgraphEvent } from '../../data-structures/event'
import { HashgraphShard } from '../../data-structures/shard'

function onReceiveGossip (shard: HashgraphShard, callback: (eventList: HashgraphEvent[]) => void, _extract?: any): void {
  shard.onReceiveGossipCallback = callback
}

export {
  onReceiveGossip
}

