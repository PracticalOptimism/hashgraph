
import { HashgraphShard } from '../../data-structures/shard'

import { sendGossip } from './send-gossip'
import { determineTransactionOrder } from './determine-transaction-order'
import { manualDeleteEventHistory } from './manual-delete-event-history'

async function manualSync (shard: HashgraphShard, targetPeerID?: string, extract?: any): Promise<void> {
  await sendGossip(shard, targetPeerID, extract)
  await determineTransactionOrder(shard, extract)
  manualDeleteEventHistory(shard, extract)
}

export {
  manualSync
}