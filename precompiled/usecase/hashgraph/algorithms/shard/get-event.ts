import { HashgraphShard } from '../../data-structures/shard'
import { HashgraphEvent } from '../../data-structures/event'

function getEvent (shard: HashgraphShard, eventID: string, _extract?: any): HashgraphEvent {
  return shard.hashgraph.eventTable[eventID]
}

export {
  getEvent
}