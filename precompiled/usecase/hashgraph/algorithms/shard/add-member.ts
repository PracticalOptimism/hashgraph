
import { HashgraphShard, HashgraphShardMember } from '../../data-structures/shard'


import { addEvent } from '../event/add-event'
import { createEvent } from '../event/create-event'
import { createMember } from '../member/create-member'
import { addMember as addMemberToHashgraph } from '../member/add-member'
import { determineRound } from '../hashgraph/determine-round'
import { sendTransaction } from './send-transaction'

import {
  HASHGRAPH_SHARD_MESSAGE_TYPE,
  HASHGRAPH_SHARD_ADD_MEMBER_NOMINATION,
  HASHGRAPH_SHARD_ERROR_PREFIX,
  MEMBER_UPDATE_TYPE_ADDED
} from '../../data-structures/shard'


async function addMember (
  shard: HashgraphShard, peerID?: string,
  isSelf?: boolean, isConnectedPeer?: boolean, candidateMember?: HashgraphShardMember,
  skipVoting?: boolean, round?: number, extract?: any): Promise<string> {

  peerID = peerID || (candidateMember ? candidateMember.peerID : '')
  let publicKey = shard.peerIDPublicKeyMap[peerID || ''] || ''

  if (candidateMember) {
    publicKey = candidateMember.publicKey

    if (!publicKey) {
      console.warn(`${HASHGRAPH_SHARD_ERROR_PREFIX} candidateMember must have 'publicKey' property when calling addMember()`)
      return ''
    }
  }

  const isAlreadyAdded = shard.hashgraph.memberTable[publicKey] ? true : false
  if (isAlreadyAdded) { return publicKey }

  const numOfConnectedPeers = Object.keys(shard.connectedPeerMap).length

  // Add self member without voting
  if (isSelf && !shard.selfPeerID) {
    const member = await createMember('', extract)
    peerID = peerID ? peerID : member.publicKey
    publicKey = member.publicKey
    shard.publicKeyPeerIDMap[publicKey] = peerID
    shard.peerIDPublicKeyMap[peerID] = member.publicKey
    shard.selfPeerID = peerID
    addMemberToHashgraph(shard.hashgraph, member, round, extract)

    // Create an initial event when adding self as a member
    const newEvent = await createEvent(member, [], '', '', extract)
    await addEvent(shard.hashgraph, member, newEvent, extract)
    determineRound(shard.hashgraph, newEvent, extract)

    if (shard.onMemberUpdateCallback) {
      shard.onMemberUpdateCallback({ publicKey, peerID }, MEMBER_UPDATE_TYPE_ADDED)
    }
    delete shard.addMemberCandidateTable[publicKey]

    if (shard.onReceiveGossipCallback) {
      shard.onReceiveGossipCallback([newEvent])
    }

  // Add member without voting when skipVoting is enabled or is second member
  } else if (skipVoting || numOfConnectedPeers < 1) {
    if (!publicKey) { return '' }
    const member = await createMember(publicKey, extract)
    peerID = peerID ? peerID : member.publicKey
    shard.publicKeyPeerIDMap[publicKey] = peerID
    shard.peerIDPublicKeyMap[peerID] = member.publicKey
    addMemberToHashgraph(shard.hashgraph, member, round, extract)

    if (isConnectedPeer) {
      shard.connectedPeerMap[peerID] = true
    }

    if (shard.onMemberUpdateCallback) {
      shard.onMemberUpdateCallback(candidateMember!, MEMBER_UPDATE_TYPE_ADDED)
    }

  // Add member through community voting to approve new member
  } else if (publicKey && shard.selfPeerID) {
    if (!candidateMember!.peerConnectionMap) { candidateMember!.peerConnectionMap = {} }
    if (isConnectedPeer) { candidateMember!.peerConnectionMap![shard.peerIDPublicKeyMap[shard.selfPeerID]] = true }

    sendTransaction(shard, {
      [HASHGRAPH_SHARD_MESSAGE_TYPE]: HASHGRAPH_SHARD_ADD_MEMBER_NOMINATION,
      candidateMember
    }, extract)
  }

  return publicKey
}

export {
  addMember
}