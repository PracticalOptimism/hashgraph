import { HashgraphShard } from '../../data-structures/shard'

import { manualDeleteEventHistory } from './manual-delete-event-history'

function startAutoDeleteEventHistory (shard: HashgraphShard, extract?: any): void {
  const deleteInterval = setInterval(() => { manualDeleteEventHistory(shard, extract) }, shard.elapsedTimeToDeleteEventHistory * 1000)

  if (shard.deleteEventHistoryInterval) {
    clearInterval(shard.deleteEventHistoryInterval as NodeJS.Timer)
  }

  shard.deleteEventHistoryInterval = deleteInterval
  shard.isStartedAutoDelete = true
}

export {
  startAutoDeleteEventHistory
}