import { HashgraphShard } from '../../data-structures/shard'

import { stopAutoDeleteEventHistory } from './stop-auto-delete-event-history'

function stopAutoSync (shard: HashgraphShard, extract?: any): void {
  if (!shard.syncInterval) {
    return
  }
  clearInterval(shard.syncInterval as NodeJS.Timer)
  shard.isStartedAutoSync = false

  stopAutoDeleteEventHistory(shard, extract)
}

export {
  stopAutoSync
}