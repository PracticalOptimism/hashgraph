import { HashgraphEvent } from '../../data-structures/event'
import { HashgraphShard } from '../../data-structures/shard'

function onSendGossip (shard: HashgraphShard, callback: (targetPeerID: string, eventList: HashgraphEvent[]) => Promise<void>|void, _extract?: any): void {
  shard.onSendGossipCallback = callback
}

export {
  onSendGossip
}