import { HashgraphShard, HashgraphShardMember } from '../../data-structures/shard'

import { getMember } from '../member/get-member'
import { removeMember as removeMemberFromHashgraph } from '../member/remove-member'
import { sendTransaction } from './send-transaction'

import {
  HASHGRAPH_SHARD_MESSAGE_TYPE,
  MEMBER_UPDATE_TYPE_REMOVED,
  HASHGRAPH_SHARD_REMOVE_MEMBER_NOMINATION
} from '../../data-structures/shard'


function removeMember (shard: HashgraphShard, candidateMember: HashgraphShardMember, skipVoting?: boolean, round?: number, extract?: any): void {
  const publicKey = candidateMember.publicKey
  const hashgraphMember = getMember(shard.hashgraph, publicKey, extract)

  // Ensure candidate is already a member.
  if (!hashgraphMember) { return }

  if (shard.hashgraph.latestRoundCreated < 2 ||  skipVoting) {
    removeMemberFromHashgraph(shard.hashgraph, hashgraphMember.uid, round, extract)

    const peerID = shard.publicKeyPeerIDMap[publicKey]
    delete shard.connectedPeerMap[peerID]

    if (shard.onMemberUpdateCallback) {
      shard.onMemberUpdateCallback(candidateMember, MEMBER_UPDATE_TYPE_REMOVED)
    }
    delete shard.removeMemberCandidateTable[publicKey]
  } else {
    sendTransaction(shard, {
      [HASHGRAPH_SHARD_MESSAGE_TYPE]: HASHGRAPH_SHARD_REMOVE_MEMBER_NOMINATION,
      candidateMember
    }, extract)
  }
}

export {
  removeMember
}