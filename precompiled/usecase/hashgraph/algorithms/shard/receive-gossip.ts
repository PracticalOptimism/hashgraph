
import { HashgraphEvent } from '../../data-structures/event'
import { HashgraphShard } from '../../data-structures/shard'

import { getMember } from '../member/get-member'
import { addEvent } from '../event/add-event'
import { determineRound } from '../hashgraph/determine-round'
import { getMemberLatestEvent } from '../member/get-member-latest-event'
import { createEvent } from '../event/create-event'

import { getMemberPublicKey } from './get-member-public-key'


async function receiveGossip (shard: HashgraphShard, eventList: HashgraphEvent[], extract?: any): Promise<void> {
  const otherParentEvent = eventList[eventList.length - 1]

  // Add sync events and other parent event to the hashgraph.
  for (const event of eventList) {
    const eventMember = getMember(shard.hashgraph, event.creatorUID, extract)
    await addEvent(shard.hashgraph, eventMember, event, extract)
    determineRound(shard.hashgraph, event, extract)
  }

  // Create new event to commemorate the sync.
  const selfPublicKey = getMemberPublicKey(shard, shard.selfPeerID, extract)
  const selfMember = getMember(shard.hashgraph, selfPublicKey, extract)
  if (!selfMember) { return }

  const latestSelfEvent = getMemberLatestEvent(shard.hashgraph, selfMember, extract)
  const latestSelfEventHash = latestSelfEvent!.uid || ''
  const otherParentHash = otherParentEvent.uid || ''

  const newEvent = await createEvent(selfMember, shard.transactionQueue.slice(), latestSelfEventHash, otherParentHash, extract)
  shard.transactionQueue = []

  // Add the new event to the hashgraph.
  await addEvent(shard.hashgraph, selfMember, newEvent, extract)
  determineRound(shard.hashgraph, newEvent, extract)

  if (!shard.onReceiveGossipCallback) { return }

  shard.onReceiveGossipCallback([newEvent].concat(eventList))
}

export {
  receiveGossip
}