import { HashgraphShard } from '../../data-structures/shard'

import { createHashgraph } from '../hashgraph/create-hashgraph'

function createShard (extract?: any): HashgraphShard {
  const hashgraph = createHashgraph(extract)
  const shard = new HashgraphShard(hashgraph)
  return shard
}

export {
  createShard
}

