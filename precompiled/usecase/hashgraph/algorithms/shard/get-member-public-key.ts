import { HashgraphShard } from '../../data-structures/shard'

function getMemberPublicKey (shard: HashgraphShard, peerID: string, _extract?: any): string {
  return shard.peerIDPublicKeyMap[peerID] || ''
}

export {
  getMemberPublicKey
}