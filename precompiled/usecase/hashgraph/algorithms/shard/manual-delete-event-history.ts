import { HashgraphShard } from '../../data-structures/shard'

import { deleteEventHistory } from '../event/delete-event-history'

function manualDeleteEventHistory (shard: HashgraphShard, extract?: any): void {
  const numOfMembers = shard.hashgraph.nPopulationSize

  if (numOfMembers < 2) { return }

  const roundsPerSecond = (Math.floor(shard.syncTickRate / 1000) * numOfMembers)
  const numRoundsEarlier = roundsPerSecond * shard.elapsedTimeToDeleteEventHistory
  deleteEventHistory(shard.hashgraph, numRoundsEarlier, extract)
}

export {
  manualDeleteEventHistory
}