import { HashgraphShard, HashgraphShardMember } from '../../data-structures/shard'

function setMinimumVotePercentToAddMember (shard: HashgraphShard, callback: (candidateMember: HashgraphShardMember) => Promise<number>|number, _extract?: any): void {
  shard.setVotePercentToAddMemberCallback = callback
}

export {
  setMinimumVotePercentToAddMember
}