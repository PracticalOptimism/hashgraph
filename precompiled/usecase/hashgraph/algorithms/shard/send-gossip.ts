import { HashgraphShard } from '../../data-structures/shard'

import { getMember } from '../member/get-member'
import { getMemberLatestEvent } from '../member/get-member-latest-event'
import { getUnknownEventListForMember } from '../member/get-unknown-event-list-for-member'

async function sendGossip (shard: HashgraphShard, targetPeerID?: string, extract?: any): Promise<void> {
  const connectedPeerList = Object.keys(shard.connectedPeerMap)
  if (connectedPeerList.length < 1) { return }

  const randomPeerID = targetPeerID || connectedPeerList[Math.floor(Math.random() * connectedPeerList.length)]
  const randomPublicKey = shard.peerIDPublicKeyMap[randomPeerID]

  // Ensure connection is established with random peer.
  if (!(shard.connectedPeerMap[randomPeerID] || shard.connectedPeerMap[randomPublicKey])) { return }

  const targetMember = getMember(shard.hashgraph, randomPublicKey, extract)
  const selfMember = getMember(shard.hashgraph, shard.peerIDPublicKeyMap[shard.selfPeerID], extract)

  const latestSelfEvent = getMemberLatestEvent(shard.hashgraph, selfMember, extract)
  if (!latestSelfEvent) { return }

  const syncEventList = getUnknownEventListForMember(shard.hashgraph, targetMember, extract)
  syncEventList.push(latestSelfEvent)

  if (!shard.onSendGossipCallback) { return }

  return shard.onSendGossipCallback!(randomPeerID, JSON.parse(JSON.stringify(syncEventList)))
}

export {
  sendGossip
}