
import { HashgraphShard } from '../../data-structures/shard'

function sendTransaction (shard: HashgraphShard, transaction: any, _extract?: any): void {
  shard.transactionQueue.push(transaction)
}

export {
  sendTransaction
}