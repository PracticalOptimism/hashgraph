
import { HashgraphShard, HashgraphShardMember } from '../../data-structures/shard'

import { isActiveMember } from '../member/is-active-member'
import { getMemberList } from '../member/get-member-list'
import { removeMember } from './remove-member'

async function onRemoveMemberVote (shard: HashgraphShard, candidateMember: HashgraphShardMember, votingMember: HashgraphShardMember, vote: boolean, roundVoted: number, extract?: any): Promise<void> {

  // Ensure there is a voting member.
  if (!votingMember) { return }

  // Ensure candidate is already active.
  if (!isActiveMember(shard.hashgraph, candidateMember.publicKey, roundVoted, extract)) { return }

  // Ensure member has been nominated.
  if (!shard.removeMemberCandidateTable[candidateMember.publicKey]) { return }

  // Submit the voting member's vote.
  shard.removeMemberCandidateTable[candidateMember.publicKey].voteTable[votingMember.publicKey] = vote

  const minimumVotePercent: number = shard.removeMemberCandidateTable[candidateMember.publicKey].minimumVotePercent
  const roundNominated: number = shard.removeMemberCandidateTable[candidateMember.publicKey].roundNominated

  const votingMemberList = getMemberList(shard.hashgraph, roundNominated, extract)

  // Count the number of yes votes.
  let numOfYesVotes: number = 0

  for (const member of votingMemberList) {
    const memberVote = shard.removeMemberCandidateTable[candidateMember.publicKey].voteTable[member.uid] ? 1 : 0
    numOfYesVotes += memberVote
  }

  const yesVotePercent: number = Math.floor((numOfYesVotes / votingMemberList.length) * 100)

  // Ensure there are enough yes votes to add the member.
  if (yesVotePercent < minimumVotePercent) { return }

  // Remove the member from the shard if they reach minimum number of required votes
  await removeMember(shard, candidateMember, true, roundVoted + 2, extract)
}

export {
  onRemoveMemberVote
}