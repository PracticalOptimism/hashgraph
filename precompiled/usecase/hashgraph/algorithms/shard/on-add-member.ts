
import { HashgraphShard, HashgraphShardMember } from '../../data-structures/shard'


function onAddMember (shard: HashgraphShard, callback: (candidateMember: HashgraphShardMember) => Promise<boolean>|boolean, _extract?: any): void {
  shard.onAddMemberCallback = callback
}

export {
  onAddMember
}