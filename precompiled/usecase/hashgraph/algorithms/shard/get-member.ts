
import { HashgraphShard, HashgraphShardMember } from '../../data-structures/shard'

import { getMemberPublicKey } from './get-member-public-key'

function getMember (shard: HashgraphShard, peerID: string, extract?: any): HashgraphShardMember {
  const memberPublicKey = getMemberPublicKey(shard, peerID, extract)
  return { publicKey: memberPublicKey, peerID }
}

export {
  getMember
}