import { HashgraphShard } from '../../data-structures/shard'

import { manualSync } from './manual-sync'
import { startAutoDeleteEventHistory } from './start-auto-delete-event-history'

function startAutoSync (shard: HashgraphShard, extract?: any): void {
  const syncInterval = setInterval(() => { manualSync(shard, extract) }, shard.syncTickRate)

  if (shard.syncInterval) {
    clearInterval(shard.syncInterval as NodeJS.Timer)
  }

  shard.syncInterval = syncInterval
  shard.isStartedAutoSync = true

  startAutoDeleteEventHistory(shard, extract)
}

export {
  startAutoSync
}