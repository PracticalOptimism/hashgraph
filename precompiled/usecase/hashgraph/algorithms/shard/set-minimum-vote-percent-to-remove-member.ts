import { HashgraphShard, HashgraphShardMember } from '../../data-structures/shard'

function setMinimumVotePercentToRemoveMember (shard: HashgraphShard, callback: (candidateMember: HashgraphShardMember) => Promise<number>|number, _extract?: any): void {
  shard.setVotePercentToRemoveMemberCallback = callback
}

export {
  setMinimumVotePercentToRemoveMember
}