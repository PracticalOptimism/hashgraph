import { HashgraphShard, HashgraphShardMember } from '../../data-structures/shard'

function getMemberList (shard: HashgraphShard, _extract?: any): HashgraphShardMember[] {
  const memberPublicKeyList = Object.keys(shard.hashgraph.memberTable).map((uid) => shard.hashgraph.memberTable[uid].publicKey)
  const shardMemberList = memberPublicKeyList.map((publicKey) => ({ publicKey, peerID: shard.publicKeyPeerIDMap[publicKey] })) as HashgraphShardMember[]
  return shardMemberList
}

export {
  getMemberList
}