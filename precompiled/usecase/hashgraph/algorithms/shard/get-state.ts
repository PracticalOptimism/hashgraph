
import { HashgraphShard } from '../../data-structures/shard'

function getState (shard: HashgraphShard, _extract?: any): any {
  return shard.stateTable[shard.latestRoundReceived] || {}
}

export {
  getState
}