import { HashgraphShard } from '../../data-structures/shard'

import { startAutoSync } from './start-auto-sync'

function setSyncTickRate (shard: HashgraphShard, tickRate: number, extract?: any): void {
  if (tickRate <= 0) {
    tickRate = 1000
  }
  shard.syncTickRate = tickRate

  // Reset the auto sync frequency
  if (shard.isStartedAutoSync) {
    startAutoSync(shard, extract)
  }
}

export {
  setSyncTickRate
}