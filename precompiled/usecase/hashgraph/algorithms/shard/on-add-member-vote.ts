
import { HashgraphShard, HashgraphShardMember } from '../../data-structures/shard'

import { getMemberList } from '../member/get-member-list'
import { isActiveMember } from '../member/is-active-member'
import { getMemberPublicKey } from './get-member-public-key'
import { addMember } from './add-member'

async function onAddMemberVote (shard: HashgraphShard, candidateMember: HashgraphShardMember, votingMember: HashgraphShardMember, vote: boolean, roundVoted: number, extract?: any): Promise<void> {

  // Ensure there is a voting member.
  if (!votingMember) { return }

  // Ensure candidate is not already active.
  if (isActiveMember(shard.hashgraph, candidateMember.publicKey, roundVoted, extract)) { return }

  // Ensure member has been nominated.
  if (!shard.addMemberCandidateTable[candidateMember.publicKey]) { return }

  // Submit the voting member's vote.
  shard.addMemberCandidateTable[candidateMember.publicKey].voteTable[votingMember.publicKey] = vote

  const minimumVotePercent: number = shard.addMemberCandidateTable[candidateMember.publicKey].minimumVotePercent
  const roundNominated: number = shard.addMemberCandidateTable[candidateMember.publicKey].roundNominated

  const votingMemberList = getMemberList(shard.hashgraph, roundNominated, extract)

  // Count the number of yes votes.
  let numOfYesVotes: number = 0

  for (const member of votingMemberList) {
    const memberVote = shard.addMemberCandidateTable[candidateMember.publicKey].voteTable[member.uid] ? 1 : 0
    numOfYesVotes += memberVote
  }

  const yesVotePercent: number = Math.floor((numOfYesVotes / votingMemberList.length) * 100)

  // Ensure there are enough yes votes to add the member.
  if (yesVotePercent < minimumVotePercent) { return }

  const selfPublicKey = getMemberPublicKey(shard, shard.selfPeerID, extract)

  let isConnectedPeer = false
  if (candidateMember.peerConnectionMap) {
    isConnectedPeer = candidateMember.peerConnectionMap[selfPublicKey]
  }

  // Add the member to the shard if they reach minimum number of required votes
  await addMember(shard, candidateMember.peerID!, false, isConnectedPeer, candidateMember, true, roundVoted + 2, extract)
}

export {
  onAddMemberVote
}