import { HashgraphShard, HashgraphShardMember } from '../../data-structures/shard'

import { isActiveMember } from '../member/is-active-member'
import { getMemberPublicKey } from './get-member-public-key'
import { sendTransaction } from './send-transaction'

import {
  HASHGRAPH_SHARD_MESSAGE_TYPE,
  HASHGRAPH_SHARD_REMOVE_MEMBER_VOTE
} from '../../data-structures/shard'


async function onRemoveMemberNomination (shard: HashgraphShard, candidateMember: HashgraphShardMember, roundNominated: number, extract?: any): Promise<void> {

  // Ensure candidate is already active.
  if (!isActiveMember(shard.hashgraph, candidateMember.publicKey, extract)) { return }

  // Ensure member hasn't already been nominated.
  if (shard.removeMemberCandidateTable[candidateMember.publicKey]) { return }

  let minimumVotePercent: number = shard.defaultMinimumVotePercent
  if (shard.setVotePercentToRemoveMemberCallback) {
    minimumVotePercent = await shard.setVotePercentToRemoveMemberCallback(candidateMember)
  }

  // Add the member as a candidate
  shard.removeMemberCandidateTable[candidateMember.publicKey] = {
    candidate: candidateMember, roundNominated, voteTable: {}, minimumVotePercent
  }

  const selfMemberPublicKey: string = getMemberPublicKey(shard, shard.selfPeerID, extract)

  if (!selfMemberPublicKey) { return }

  let selfMemberVote: boolean = true
  if (shard.onRemoveMemberCallback) {
    selfMemberVote = await shard.onRemoveMemberCallback(candidateMember)
  }

  // Send a vote
  sendTransaction(shard, {
    [HASHGRAPH_SHARD_MESSAGE_TYPE]: HASHGRAPH_SHARD_REMOVE_MEMBER_VOTE,
    candidateMember, votingMember: { publicKey: selfMemberPublicKey }, vote: selfMemberVote
  }, extract)
}

export {
  onRemoveMemberNomination
}