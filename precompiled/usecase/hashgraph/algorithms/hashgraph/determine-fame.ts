import { Hashgraph, VoteTable } from '../../data-structures/hashgraph'
import { HashgraphEvent } from '../../data-structures/event'

import { getMemberList } from '../member/get-member-list'
import { stronglySeenRoundWitnesses } from '../event/strongly-seen-round-witnesses'
import { canSeeEvent } from '../event/can-see-event'
import { middleBitOfSignature } from './middle-bit-of-signature'

function determineFame (hashgraph: Hashgraph, extract?: any): void {
  const roundStartIndex = hashgraph.latestRoundReceived
  const roundEndIndex = hashgraph.latestRoundCreated

  const voteTable: VoteTable = {}

  for (let i = roundStartIndex; i < roundEndIndex; i++) {
    if (!hashgraph.eventRoundCreatedTable[i]) { continue }

    const roundWitnessList = Object.keys(hashgraph.eventRoundCreatedTable[i].witnessEventTable).map((eventUID) => hashgraph.eventTable[eventUID])

    const numActiveMembers = getMemberList(hashgraph, i, extract).length
    // Filter out the rounds where all witnesses have already been calculated as famous or not famous
    let isFameDecidedForAll: boolean = numActiveMembers === roundWitnessList.length
    for (const eventX of roundWitnessList) {
      if (!eventX.isFameDecided) {
        isFameDecidedForAll = false
        break
      }
    }

    if (isFameDecidedForAll) {
      hashgraph.latestRoundReceived = i
      continue
    }

    for (let j = i + 1; j <= roundEndIndex; j++) {
      if (!hashgraph.eventRoundCreatedTable[j]) { continue }

      const laterRoundWitnessList = Object.keys(hashgraph.eventRoundCreatedTable[j].witnessEventTable).map((eventUID) => hashgraph.eventTable[eventUID])

      for (const eventX of roundWitnessList) {
        if (eventX.isFameDecided) {
          continue
        }

        if (!voteTable[eventX.uid]) {
          voteTable[eventX.uid] = {
            numOfUniqueMemberVoteYes: 0, numOfUniqueMemberVoteNo: 0, numOfUniqueMemberVoters: 0, voteRecord: {}, activeMemberTable: hashgraph.eventRoundCreatedTable[i].memberTable
          }
        }

        for (const eventY of laterRoundWitnessList) {
          if (eventX.isFameDecided) {
            break
          }

          const roundDiff = eventY.roundCreated - eventX.roundCreated
          const stronglySeenWitnesses = stronglySeenRoundWitnesses(hashgraph, eventY, eventY.roundCreated - 1, extract)

          let numVoteYes = voteTable[eventX.uid].numOfUniqueMemberVoteYes
          let numVoteNo = voteTable[eventX.uid].numOfUniqueMemberVoteNo
          for (const witness of stronglySeenWitnesses) {
            const isEventSeen = canSeeEvent(hashgraph, witness, eventX, extract)
            numVoteYes += isEventSeen ? 1 : 0
            numVoteNo += !isEventSeen ? 1 : 0
          }

          const majorityVote = numVoteYes >= numVoteNo
          const numMajorityVoters = majorityVote ? numVoteYes : numVoteNo

          if (!voteTable[eventX.uid].voteRecord[eventY.creatorUID]) {
            voteTable[eventX.uid].voteRecord[eventY.creatorUID] = { hasAnEventVotedYes: false, hasAnEventVotedNo: false, eventTable: {} }
          }

          if (!voteTable[eventX.uid].voteRecord[eventY.creatorUID].eventTable[eventY.uid]) {
            voteTable[eventX.uid].voteRecord[eventY.creatorUID].eventTable[eventY.uid] = { vote: false, hasVoted: false }
          }

          // This is the case for the first round in the election.
          if (roundDiff === 1) {
            // y.vote <- can y see x?
            const vote = canSeeEvent(hashgraph, eventY, eventX, extract)
            voteForEvent(voteTable, eventX, eventY, vote, extract)
          } else {
            // This is the case of a normal round.
            if (roundDiff % hashgraph.cFrequencyOfCoinRounds > 0) {
              if (numMajorityVoters >= Math.ceil((2 / 3) * numActiveMembers)) {
                decideForEvent(hashgraph, eventX, majorityVote, extract)
                voteForEvent(voteTable, eventX, eventY, majorityVote, extract) // y.vote <- v
                break
              } else {
                voteForEvent(voteTable, eventX, eventY, majorityVote, extract) // y.vote <- v
              }

            // This is the case of a coin round.
            } else {
              if (numMajorityVoters >= Math.ceil((2 / 3) * numActiveMembers)) {
                voteForEvent(voteTable, eventX, eventY, majorityVote, extract) // y.vote <- v
              } else {
                // y.vote <- middle bit of y signature
                voteForEvent(voteTable, eventX, eventY, middleBitOfSignature(eventY.signature, extract) === 1 ? true : false, extract)
              }
            }
          }
        }
      }
    }
  }
}

function voteForEvent (voteTable: VoteTable, candidateEvent: HashgraphEvent, voterEvent: HashgraphEvent, vote: boolean, _extract?: any): void {

  if (!voteTable[candidateEvent.uid].voteRecord[voterEvent.creatorUID].eventTable[voterEvent.uid].hasVoted) {
    const isFromActiveMember = voteTable[candidateEvent.uid].activeMemberTable[voterEvent.creatorUID] ? true : false
    const hasMemberNotVotedYes = !voteTable[candidateEvent.uid].voteRecord[voterEvent.creatorUID].hasAnEventVotedYes
    const hasMemberNotVotedNo = !voteTable[candidateEvent.uid].voteRecord[voterEvent.creatorUID].hasAnEventVotedNo

    if (vote === true && hasMemberNotVotedYes && isFromActiveMember) {
      voteTable[candidateEvent.uid].numOfUniqueMemberVoteYes++
      voteTable[candidateEvent.uid].voteRecord[voterEvent.creatorUID].hasAnEventVotedYes = true
    } else if (vote === false && hasMemberNotVotedNo && isFromActiveMember) {
      voteTable[candidateEvent.uid].numOfUniqueMemberVoteNo++
      voteTable[candidateEvent.uid].voteRecord[voterEvent.creatorUID].hasAnEventVotedNo = true
    }

    const hasNotVoted = hasMemberNotVotedYes && hasMemberNotVotedNo && isFromActiveMember
    if (hasNotVoted) {
      voteTable[candidateEvent.uid].numOfUniqueMemberVoters++
    }

    voteTable[candidateEvent.uid].voteRecord[voterEvent.creatorUID].eventTable[voterEvent.uid].vote = vote
    voteTable[candidateEvent.uid].voteRecord[voterEvent.creatorUID].eventTable[voterEvent.uid].hasVoted = true
  }
}

function decideForEvent (hashgraph: Hashgraph, candidateEvent: HashgraphEvent, decision: boolean, _extract?: any): void {
  candidateEvent.isFamous = decision
  candidateEvent.isFameDecided = true

  if (decision) {
    hashgraph.eventRoundCreatedTable[candidateEvent.roundCreated].famousEventTable[candidateEvent.uid] = true
  } else {
    hashgraph.eventRoundCreatedTable[candidateEvent.roundCreated].notFamousEventTable[candidateEvent.uid] = true
  }
}

export {
  determineFame,
  voteForEvent
}