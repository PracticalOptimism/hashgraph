import { Hashgraph } from '../../data-structures/hashgraph'
import { HashgraphEvent } from '../../data-structures/event'

import { selfParentEvent } from '../event/self-parent-event'
import { otherParentEvent } from '../event/other-parent-event'
import { stronglySeenRoundWitnesses } from '../event/strongly-seen-round-witnesses'

function determineRound (hashgraph: Hashgraph, event: HashgraphEvent, extract?: any): void {

  // Ensure round hasn't already been calculated.
  if (event.roundCreated > 0) { return }

  let round = 1
  const selfParent = selfParentEvent(hashgraph, event, extract)
  const otherParent = otherParentEvent(hashgraph, event, extract)

  if (selfParent) {
    round = Math.max(round, selfParent.roundCreated)
  }

  if (otherParent) {
    round = Math.max(round, otherParent.roundCreated)
  }

  const numStronglySeenRoundWitnesses = stronglySeenRoundWitnesses(hashgraph, event, round, extract).length

  event.roundCreated = numStronglySeenRoundWitnesses >= Math.ceil((2 / 3) * hashgraph.nPopulationSize) ? round + 1 : round

  event.isWitness = !selfParent && !otherParent
  if (selfParent) {
    event.isWitness = event.roundCreated > selfParent.roundCreated
  }

  if (!hashgraph.eventRoundCreatedTable[event.roundCreated]) {
    hashgraph.eventRoundCreatedTable[event.roundCreated] = {
      memberTable: {}, eventTable: {}, witnessEventTable: {}, famousEventTable: {}, notFamousEventTable: {}
    }

    hashgraph.eventRoundCreatedTable[event.roundCreated].memberTable[event.creatorUID] = true
  }

  hashgraph.eventRoundCreatedTable[event.roundCreated].eventTable[event.uid] = true

  if (event.isWitness) {
    hashgraph.eventRoundCreatedTable[event.roundCreated].witnessEventTable[event.uid] = true
  }

  if (event.roundCreated > hashgraph.latestRoundCreated) {
    hashgraph.latestRoundCreated = event.roundCreated
  }
}

export {
  determineRound
}