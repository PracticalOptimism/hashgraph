

// Extract the middle bit (ie. 0 or 1) of a cryptographic signature.
// This is a good deterministic pseudo-random number generator.
function middleBitOfSignature (signature: string, _extract?: any): number {
  let bit: number
  let binaryString: string = ''

  for (let i = 0; i < signature.length; i++) {
    binaryString += signature[i].charCodeAt(0).toString(2)
  }

  binaryString = binaryString.substr(binaryString.length / 2, 1)

  bit = parseInt(binaryString, 10)
  return bit
}

export {
  middleBitOfSignature
}