import { Hashgraph } from '../../data-structures/hashgraph'
import { HashgraphEvent } from '../../data-structures/event'

import { canSeeEvent } from '../event/can-see-event'
import { middleBitOfSignature } from './middle-bit-of-signature'

function determineOrder (hashgraph: Hashgraph, earliestRoundReceived?: number, extract?: any): HashgraphEvent[] {
  const roundList = Object.keys(hashgraph.eventRoundCreatedTable).map((round: string) => parseInt(round, 10))
                    .filter((round: number) => (round > hashgraph.earliestRoundCreatedWithKnownConsensus)).sort((a, b) => a - b)
  const roundStartIndex = roundList[0]
  const roundEndIndex = roundList[roundList.length - 1]

  for (let i = roundStartIndex; i < roundEndIndex; i++) {
    if (!hashgraph.eventRoundCreatedTable[i]) { continue }

    const eventList = Object.keys(hashgraph.eventRoundCreatedTable[i].eventTable).map((eventUID) => hashgraph.eventTable[eventUID])

    for (let j = i + 1; j < roundEndIndex; j++) {
      if (!hashgraph.eventRoundCreatedTable[j]) { continue }

      if (!hashgraph.eventRoundReceivedTable[j]) {
        hashgraph.eventRoundReceivedTable[j] = { eventTable: {} }
      }

      const laterWitnessEventList = Object.keys(hashgraph.eventRoundCreatedTable[j].witnessEventTable).map((eventUID) => hashgraph.eventTable[eventUID])

      // Ensure that fame has been decided for witness events in round j.
      let isFameDecidedForAll = true
      for (const laterWitnessEvent of laterWitnessEventList) {
        if (!laterWitnessEvent.isFameDecided) {
          isFameDecidedForAll = false
          break
        }
      }

      if (!isFameDecidedForAll) {
        continue // TODO: should this be a break? It would assume that later rounds don't have witnesses where fame has been decided for all.
      }

      const famousEventList = Object.keys(hashgraph.eventRoundCreatedTable[j].famousEventTable).map((eventUID) => hashgraph.eventTable[eventUID])

      let numOfEventsWithKnownConsensus = 0

      for (const eventX of eventList) {
        if (eventX.consensusTimestamp) {
          numOfEventsWithKnownConsensus++
          continue
        }

        // Ensure all famous events in round j can see eventX.
        let isSeenByAllFamousEvents = true
        for (const famousEvent of famousEventList) {
          const isSeenEvent = canSeeEvent(hashgraph, famousEvent, eventX, extract)
          if (!isSeenEvent) {
            isSeenByAllFamousEvents = false
            break
          }
        }

        if (!isSeenByAllFamousEvents) {
          continue
        }

        if (!hashgraph.eventRoundReceivedTable[j].eventTable[eventX.uid]) {
          eventX.roundReceived = j
          hashgraph.eventRoundReceivedTable[j].eventTable[eventX.uid] = { medianTimestamp: '', timestampList: [] }
        }

        // Collect the timestamps from the earliest events that see eventX from each member.
        let timestampList: string[] = []

        for (const famousEvent of famousEventList) {
          let earliestEventOrderIndex: number = hashgraph.eventTable[famousEvent.uid].orderIndex

          for (let k = earliestEventOrderIndex; k >= 0; k--) {
            const earlierEventUID = hashgraph.memberTable[famousEvent.creatorUID].orderedEventUidList[k]
            const earlierEvent = hashgraph.eventTable[earlierEventUID]

            const isSeenEventX = canSeeEvent(hashgraph, earlierEvent, eventX, extract)

            earliestEventOrderIndex = isSeenEventX ? k : k + 1
            if (!isSeenEventX) { break }
          }

          const earliestEventUID = hashgraph.memberTable[famousEvent.creatorUID].orderedEventUidList[earliestEventOrderIndex]
          const earliestEventToSeeEventX = hashgraph.eventTable[earliestEventUID]

          timestampList.push(earliestEventToSeeEventX.timestamp)
        }

        timestampList = timestampList.sort((a, b) => parseInt(a, 10) - parseInt(b, 10))

        const middleIndex = Math.floor(famousEventList.length / 2)
        const medianTimestamp = timestampList[middleIndex]

        hashgraph.eventRoundReceivedTable[j].eventTable[eventX.uid].medianTimestamp = medianTimestamp
        hashgraph.eventRoundReceivedTable[j].eventTable[eventX.uid].timestampList = timestampList
        eventX.consensusTimestamp = medianTimestamp

        numOfEventsWithKnownConsensus++
      }

      if (numOfEventsWithKnownConsensus === eventList.length) {
        hashgraph.earliestRoundCreatedWithKnownConsensus = i
      }
    }
  }

  let orderedEventList: HashgraphEvent[] = []

  const roundReceivedList = Object.keys(hashgraph.eventRoundReceivedTable).map((round: string) => parseInt(round, 10))
                            .filter((round: number) => (round > (earliestRoundReceived || 0))).sort((a, b) => a - b)
  const roundReceivedStartIndex = roundReceivedList[0]
  const roundReceivedEndIndex = roundReceivedList[roundReceivedList.length - 1]

  // Sort events by round received
  for (let i = roundReceivedStartIndex; i < roundReceivedEndIndex; i++) {
    const eventList = Object.keys(hashgraph.eventRoundReceivedTable[i].eventTable).map((eventUID) => hashgraph.eventTable[eventUID])
    .sort((eventA: HashgraphEvent, eventB: HashgraphEvent) => (parseInt(eventA.consensusTimestamp, 10) - parseInt(eventB.consensusTimestamp, 10)))

    orderedEventList = orderedEventList.concat(eventList)
  }

  // Sort events by median timestamp, then extended median timestamp, then whitened signature
  orderedEventList = orderedEventList.sort((eventA: HashgraphEvent, eventB: HashgraphEvent) => {
    const timeA = parseInt(eventA.consensusTimestamp, 10)
    const timeB = parseInt(eventB.consensusTimestamp, 10)

    // Sort by median timestamp
    if (timeA !== timeB) {
      return timeA - timeB
    } else {

      // Sort by extended median timestamp
      const timestampListA = hashgraph.eventRoundReceivedTable[eventA.roundReceived].eventTable[eventA.uid].timestampList
      const timestampListB = hashgraph.eventRoundReceivedTable[eventB.roundReceived].eventTable[eventB.uid].timestampList

      const shorterTimestampList = timestampListA.length <= timestampListB.length ? timestampListA : timestampListB

      const middleIndex = Math.floor(shorterTimestampList.length / 2)

      for (let l = 0; l <= middleIndex; l++) {
        const leftTimestampA = timestampListA[middleIndex - l]
        const leftTimestampB = timestampListB[middleIndex - l]

        if ((leftTimestampA && leftTimestampB) && (leftTimestampA !== leftTimestampB)) {
          return (parseInt(leftTimestampA, 10) - parseInt(leftTimestampB, 10))
        }

        const rightTimestampA = timestampListA[middleIndex + l]
        const rightTimestampB = timestampListB[middleIndex + l]

        if ((rightTimestampA && rightTimestampB) && (rightTimestampA !== rightTimestampB)) {
          return (parseInt(rightTimestampA, 10) - parseInt(rightTimestampB, 10))
        }
      }

      // Sort by whitened signature
      const signatureA = stringToBinary(eventA.signature, extract) ^ middleBitOfSignature(eventA.signature, extract) // tslint:disable-line: no-bitwise
      const signatureB = stringToBinary(eventB.signature, extract) ^ middleBitOfSignature(eventB.signature, extract) // tslint:disable-line: no-bitwise

      return signatureA - signatureB
    }
  })

  return orderedEventList
}

function stringToBinary (value: string, _extract?: any): number {
  const result: string[] = []
  for (let i = 0; i < value.length; i++) {
    result.push(value.charCodeAt(i).toString(2))
  }
  return parseInt(result.join(), 10)
}

export {
  determineOrder
}