import { Hashgraph } from '../../data-structures/hashgraph'

function createHashgraph (_extract?: any): Hashgraph {
  const hashgraph = new Hashgraph()
  hashgraph.uid = Math.random().toString()
  return hashgraph
}

export {
  createHashgraph
}

