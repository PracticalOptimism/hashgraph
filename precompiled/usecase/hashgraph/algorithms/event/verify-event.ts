import { HashgraphEvent } from '../../data-structures/event'
import { HashgraphMember } from '../../data-structures/member'

import { extractProvider } from '../../../../provider/extract'

async function verifyEvent (member: HashgraphMember, event: HashgraphEvent, extract?: any): Promise<boolean> {
  const isValidSignature = await verifyEventSignature(member, event, extract)
  const isValidHash = await checkEventHash(event, extract)
  return isValidSignature && isValidHash
}

async function verifyEventSignature (member: HashgraphMember, event: HashgraphEvent, extract?: any): Promise<boolean> {
  const eventObject = {
    payload: event.payload,
    creatorUID: event.creatorUID,
    selfParentHash: event.selfParentHash,
    otherParentHash: event.otherParentHash,
    timestamp: event.timestamp
  }
  const eventString = JSON.stringify(eventObject)
  const messageBuffer = Buffer.from(eventString)
  const { cryptography } = extractProvider(extract)
  const isValid = (await cryptography.verifyMessage(messageBuffer, event.signature, member.publicKey, extract))[0]
  return isValid
}

async function checkEventHash (event: HashgraphEvent, extract?: any): Promise<boolean> {
  const eventObject = {
    payload: event.payload,
    creatorUID: event.creatorUID,
    selfParentHash: event.selfParentHash,
    otherParentHash: event.otherParentHash,
    timestamp: event.timestamp,
    signature: event.signature
  }
  const { hashFunction } = extractProvider(extract)
  const eventHash = await Promise.resolve((await hashFunction.sha1Hash(eventObject, extract))[0])
  const isUntamperedEvent = eventHash === event.uid
  return isUntamperedEvent
}

export {
  verifyEvent
}