import { Hashgraph } from '../../data-structures/hashgraph'
import { HashgraphEvent } from '../../data-structures/event'
import { HashgraphMember } from '../../data-structures/member'

import { verifyEvent } from './verify-event'

const onSelfParentOrderFoundListenerMap: { [selfParentEventUID: string]: any } = {}

async function addEvent (hashgraph: Hashgraph, member: HashgraphMember, event: HashgraphEvent, extract?: any): Promise<boolean> {

  if (!event) { return false }

  // Ensure event isn't already added.
  if (hashgraph.eventTable[event.uid]) { return false }

  // Ensure event is valid
  const isValidEvent = await verifyEvent(member, event, extract)
  if (!isValidEvent) { return false }

  // Reset voting properties to default settings.
  event.orderIndex = -1
  event.roundCreated = -1
  event.roundReceived = -1
  event.consensusTimestamp = ''
  event.isWitness = false
  event.isFamous = false
  event.isFameDecided = false

  hashgraph.eventTable[event.uid] = event

  const eventOrderIndex = findEventOrderIndex(hashgraph, event, extract)

  // Ensure event is added after its self parent
  if (eventOrderIndex < 0) {
    const unorderedEventIndex = member.unorderedEventUidList.length
    member.unorderedEventUidList.push(event.uid)

    onSelfParentOrderFoundListenerMap[event.selfParentHash] = (selfParentOrderIndex: number) => {
      if (hashgraph.eventTable[event.uid].orderIndex >= 0) { return }

      member.unorderedEventUidList.splice(unorderedEventIndex, 1)
      hashgraph.eventTable[event.uid].orderIndex = selfParentOrderIndex + 1
      member.orderedEventUidList.splice(selfParentOrderIndex + 1, 0, event.uid)

      if (onSelfParentOrderFoundListenerMap[event.uid]) {
        onSelfParentOrderFoundListenerMap[event.uid](selfParentOrderIndex + 1)
      }
    }
  // Set the order index of the event
  } else if (eventOrderIndex >= 0) {
    if (hashgraph.eventTable[event.uid].orderIndex >= 0) { return false }

    hashgraph.eventTable[event.uid].orderIndex = eventOrderIndex
    member.orderedEventUidList.splice(eventOrderIndex, 0, event.uid)

    if (onSelfParentOrderFoundListenerMap[event.uid]) {
      onSelfParentOrderFoundListenerMap[event.uid](eventOrderIndex)
    }
  }

  return true
}

function findEventOrderIndex (hashgraph: Hashgraph, event: HashgraphEvent, _extract?: any) {
  let orderIndex: number = -1
  let selfParentOrderIndex: number = -1

  if (!event.selfParentHash) {
    orderIndex = 0
  } else if (hashgraph.eventTable[event.selfParentHash]) {
    selfParentOrderIndex = hashgraph.eventTable[event.selfParentHash].orderIndex
  }

  if (selfParentOrderIndex >= 0) {
    orderIndex = selfParentOrderIndex + 1
  }

  return orderIndex
}

export {
  addEvent,
  findEventOrderIndex
}