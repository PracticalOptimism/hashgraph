import { Hashgraph } from '../../data-structures/hashgraph'
import { HashgraphEvent } from '../../data-structures/event'

import { getMemberList } from '../member/get-member-list'
import { canSeeEventThroughMany } from './can-see-event'

function stronglySeenRoundWitnesses (hashgraph: Hashgraph, event: HashgraphEvent, round: number, extract?: any): HashgraphEvent[] {
  if (!hashgraph.eventRoundCreatedTable[round]) {
    return []
  }

  const witnessEventList = Object.keys(hashgraph.eventRoundCreatedTable[round].witnessEventTable).map((eventUID) => hashgraph.eventTable[eventUID])
  // const memberUIDList = Object.keys(hashgraph.eventRoundCreatedTable[round].memberTable)
  const memberUIDList = getMemberList(hashgraph, round, extract).map((member) => member.uid)
  // const memberUIDList = Object.keys(hashgraph.memberTable)

  const stronglySeenWitnessList: HashgraphEvent[] = []

  for (let i = 0; i < witnessEventList.length; i++) {
    const witnessEvent = witnessEventList[i]

    const isStronglySeenWitness = canSeeEventThroughMany(hashgraph, event, witnessEvent, memberUIDList, extract)

    if (isStronglySeenWitness) {
      stronglySeenWitnessList.push(witnessEvent)
    }
  }

  return stronglySeenWitnessList
}

export {
  stronglySeenRoundWitnesses
}