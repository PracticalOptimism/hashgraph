
import { HashgraphMember } from '../../data-structures/member'
import { HashgraphEvent } from '../../data-structures/event'

import { extractProvider } from '../../../../provider/extract'

async function createEvent (member: HashgraphMember, payload: any, selfParentHash?: string, otherParentHash?: string, extract?: any): Promise<HashgraphEvent> {
  const hashgraphEvent = new HashgraphEvent(payload, member.uid, selfParentHash, otherParentHash)

  const signature = await signEvent(member, hashgraphEvent, extract)
  hashgraphEvent.signature = signature

  const eventHash = await generateEventHash(hashgraphEvent, extract)
  hashgraphEvent.uid = eventHash

  return hashgraphEvent
}

async function signEvent (member: HashgraphMember, event: HashgraphEvent, extract?: any): Promise<any> {
  const eventObject = {
    payload: event.payload,
    creatorUID: event.creatorUID,
    selfParentHash: event.selfParentHash,
    otherParentHash: event.otherParentHash,
    timestamp: event.timestamp
  }
  const eventString = JSON.stringify(eventObject)
  const messageBuffer = Buffer.from(eventString)
  const { cryptography } = extractProvider(extract)
  const signature = (await cryptography.signMessage(messageBuffer, member.privateKey))[0]
  return signature
}

async function generateEventHash (event: HashgraphEvent, extract?: any): Promise<string> {
  const eventObject = {
    payload: event.payload,
    creatorUID: event.creatorUID,
    selfParentHash: event.selfParentHash,
    otherParentHash: event.otherParentHash,
    timestamp: event.timestamp,
    signature: event.signature
  }
  const { hashFunction } = extractProvider(extract)
  const eventHash = await Promise.resolve((await hashFunction.sha1Hash(eventObject))[0])
  return eventHash
}

export {
  createEvent
}