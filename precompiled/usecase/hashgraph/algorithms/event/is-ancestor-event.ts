import { Hashgraph } from '../../data-structures/hashgraph'
import { HashgraphEvent } from '../../data-structures/event'

import { isSelfAncestorEvent } from './is-self-ancestor-event'

function isAncestorEvent (hashgraph: Hashgraph, sourceEvent: HashgraphEvent, targetEvent: HashgraphEvent, extract?: any): boolean {
  const isSelfAncestor = isSelfAncestorEvent(hashgraph, sourceEvent, targetEvent, extract)

  if (isSelfAncestor) {
    return true
  }

  const ancestorEvent = highestAncestorEventFromMember(hashgraph, sourceEvent, targetEvent, extract)
  let isOtherAncestor = false
  if (ancestorEvent) {
    isOtherAncestor = isSelfAncestorEvent(hashgraph, ancestorEvent, targetEvent, extract)
  }

  return isOtherAncestor
}

function highestAncestorEventFromMember (hashgraph: Hashgraph, sourceEvent: HashgraphEvent, targetEvent: HashgraphEvent, _extract?: any): HashgraphEvent|null {

  const eventQueue: string[] = []

  const isVisitedEventTable: { [eventUID: string]: boolean } = {}
  let highestAncestorByMember: HashgraphEvent|null = null
  let highestOrderIndex: number = 0

  eventQueue.unshift(sourceEvent.selfParentHash)
  eventQueue.unshift(sourceEvent.otherParentHash)

  while (eventQueue.length > 0) {
    const hashgraphEventUID = eventQueue.pop()
    if (!hashgraphEventUID || isVisitedEventTable[hashgraphEventUID]) {
      continue
    }

    isVisitedEventTable[hashgraphEventUID] = true
    const hashgraphEvent = hashgraph.eventTable[hashgraphEventUID]

    if (!hashgraphEvent) { continue }

    if (hashgraphEvent.roundCreated < targetEvent.roundCreated) {
      continue
    }

    if (hashgraphEvent.creatorUID === targetEvent.creatorUID) {
      if (highestAncestorByMember) {
        const orderIndex = hashgraph.eventTable[hashgraphEvent.uid].orderIndex
        if (orderIndex > highestOrderIndex) {
          highestAncestorByMember = hashgraphEvent
          highestOrderIndex = hashgraph.eventTable[hashgraphEvent.uid].orderIndex
        }
      } else {
        highestAncestorByMember = hashgraphEvent
        highestOrderIndex = hashgraph.eventTable[hashgraphEvent.uid].orderIndex
      }
    }

    if (hashgraphEvent.selfParentHash) {
      eventQueue.unshift(hashgraphEvent.selfParentHash)
    }

    if (hashgraphEvent.otherParentHash) {
      eventQueue.unshift(hashgraphEvent.otherParentHash)
    }
  }

  return highestAncestorByMember
}

export {
  isAncestorEvent
}