import { Hashgraph } from '../../data-structures/hashgraph'
import { HashgraphEvent } from '../../data-structures/event'


function otherParentEvent (hashgraph: Hashgraph, event: HashgraphEvent, _extract?: any): HashgraphEvent|null {
  const otherParentEventUID = event.otherParentHash
  if (!otherParentEventUID) {
    return null
  }

  return hashgraph.eventTable[otherParentEventUID]
}

export {
  otherParentEvent
}