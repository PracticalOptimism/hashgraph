import { Hashgraph } from '../../data-structures/hashgraph'

function deleteEventHistory (hashgraph: Hashgraph, numRoundsEarlier: number, _extract?: any): void {
  const latestRound = hashgraph.latestRoundCreated - numRoundsEarlier
  const roundList = Object.keys(hashgraph.eventRoundCreatedTable).map((round: string) => parseInt(round, 10))
                    .filter((round: number) => (round < latestRound)).sort((a, b) => a - b)

  for (const round of roundList) {
    const roundCreatedEventUidList = Object.keys(hashgraph.eventRoundCreatedTable[round].eventTable)
    let roundReceivedEventUidList: string[] = []

    if (hashgraph.eventRoundReceivedTable[round]) {
      roundReceivedEventUidList = Object.keys(hashgraph.eventRoundReceivedTable[round].eventTable)
    }

    for (const eventUID of roundReceivedEventUidList) {
      delete hashgraph.eventTable[eventUID]
    }

    for (const eventUID of roundCreatedEventUidList) {
      delete hashgraph.eventTable[eventUID]
    }

    delete hashgraph.eventRoundCreatedTable[round]
    delete hashgraph.eventRoundReceivedTable[round]
  }
}

export {
  deleteEventHistory
}