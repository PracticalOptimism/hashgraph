import { Hashgraph } from '../../data-structures/hashgraph'
import { HashgraphEvent } from '../../data-structures/event'

function selfParentEvent (hashgraph: Hashgraph, event: HashgraphEvent, _extract?: any): HashgraphEvent|null {

  if (!hashgraph.memberTable[event.creatorUID]) { return null }

  const selfParentEventUID = event.selfParentHash

  if (!selfParentEventUID) {
    return null
  }

  return hashgraph.eventTable[selfParentEventUID]
}

export {
  selfParentEvent
}