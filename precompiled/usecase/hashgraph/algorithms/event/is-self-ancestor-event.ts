import { Hashgraph } from '../../data-structures/hashgraph'
import { HashgraphEvent } from '../../data-structures/event'

function isSelfAncestorEvent (hashgraph: Hashgraph, sourceEvent: HashgraphEvent, targetEvent: HashgraphEvent, _extract?: any): boolean {
  if (!sourceEvent || !targetEvent) {
    return false
  }
  if (sourceEvent.uid === targetEvent.uid) {
    return true
  }
  if (sourceEvent.creatorUID !== targetEvent.creatorUID) {
    return false
  }
  const sourceEventOrderIndex = hashgraph.eventTable[sourceEvent.uid].orderIndex
  const targetEventOrderIndex = hashgraph.eventTable[targetEvent.uid].orderIndex

  return sourceEventOrderIndex > targetEventOrderIndex
}

export {
  isSelfAncestorEvent
}