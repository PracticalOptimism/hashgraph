import { Hashgraph, EventPathTable } from '../../data-structures/hashgraph'
import { HashgraphEvent } from '../../data-structures/event'

import { isAncestorEvent } from './is-ancestor-event'
import { isSelfAncestorEvent } from './is-self-ancestor-event'

function canSeeEvent (hashgraph: Hashgraph, sourceEvent: HashgraphEvent, targetEvent: HashgraphEvent, extract?: any): boolean {
  return isAncestorEvent(hashgraph, sourceEvent, targetEvent, extract)
}

function canSeeEventThroughMany (hashgraph: Hashgraph, sourceEvent: HashgraphEvent, targetEvent: HashgraphEvent, memberUIDList: string[], extract?: any): boolean {
  return hasManyPathsToEvent(hashgraph, sourceEvent, targetEvent, memberUIDList, extract)
}

function hasManyPathsToEvent (hashgraph: Hashgraph, sourceEvent: HashgraphEvent, targetEvent: HashgraphEvent, memberUIDList: string[], extract?: any): boolean {

  const eventQueue: string[] = []
  const eventPathTable: EventPathTable = {}

  const isVisitedEventTable: { [eventUID: string]: boolean } = {}
  const isVisitedMemberTable: { [memberUID: string]: boolean } = {}

  let nearestAncestorByMember: HashgraphEvent|null = null

  isVisitedMemberTable[sourceEvent.creatorUID] = true

  eventPathTable[sourceEvent.selfParentHash] = {
    hasTargetAsAncestor: false, isAncestorRelationKnown: false, childEventList: [sourceEvent.uid]
  }

  eventPathTable[sourceEvent.otherParentHash] = {
    hasTargetAsAncestor: false, isAncestorRelationKnown: false, childEventList: [sourceEvent.uid]
  }

  eventQueue.unshift(sourceEvent.selfParentHash)
  eventQueue.unshift(sourceEvent.otherParentHash)


  while (eventQueue.length > 0) {
    const hashgraphEventUID = eventQueue.pop()
    if (!hashgraphEventUID || isVisitedEventTable[hashgraphEventUID]) {
      continue
    }

    isVisitedEventTable[hashgraphEventUID] = true
    const hashgraphEvent = hashgraph.eventTable[hashgraphEventUID]

    if (!hashgraphEvent) { continue }

    if (hashgraphEvent.roundCreated < targetEvent.roundCreated) {
      continue
    }

    // Record the events that have been visited by the current event if the target event
    // is a self ancestor. Break from the loop if the target event is not a self ancestor
    // of the current event by the same member.
    if (hashgraphEvent.creatorUID === targetEvent.creatorUID) {
      if (!nearestAncestorByMember) {
        nearestAncestorByMember = hashgraphEvent

        const isTargetSelfAncestorOfNearest = isSelfAncestorEvent(hashgraph, nearestAncestorByMember, targetEvent, extract)
        if (!isTargetSelfAncestorOfNearest) {
          break
        }
      }

      const isTargetSelfAncestor = isSelfAncestorEvent(hashgraph, hashgraphEvent, targetEvent, extract)
      eventPathTable[hashgraphEvent.uid].hasTargetAsAncestor = isTargetSelfAncestor
      eventPathTable[hashgraphEvent.uid].isAncestorRelationKnown = true

      if (isTargetSelfAncestor) {
        isVisitedMemberTable[hashgraphEvent.creatorUID] = true
        updateChildEventAlongPathsFromSourceEvent(hashgraph, eventPathTable, hashgraphEvent.uid, isVisitedMemberTable, extract)
      } else {
        continue
      }
    }

    // Set the self parent event to have the same member list as the current event. Also check if the self
    // parent event is known to have the target as an ancestor to ensure the current event's member list is
    // recorded/visited.
    if (hashgraphEvent.selfParentHash) {
      if (!eventPathTable[hashgraphEvent.selfParentHash]) {
        eventPathTable[hashgraphEvent.selfParentHash] = { hasTargetAsAncestor: false, isAncestorRelationKnown: false, childEventList: [] }
      }
      eventPathTable[hashgraphEvent.selfParentHash].childEventList.push(hashgraphEvent.uid)

      // If the self parent event has already been visited, and the ancestor relation is known to be true, then
      // update the child events of the current event to also see/know about the target ancestor event.
      //
      // If the self parent event does not yet know if the target event is an ancestor, because we have
      // added this event as a child, if the target event turns out to be an ancestor, then it will traverse
      // through its child events and know about the child events of this current event as well.
      if (isVisitedEventTable[hashgraphEvent.selfParentHash]) {
        if (eventPathTable[hashgraphEvent.selfParentHash].isAncestorRelationKnown) {
          if (eventPathTable[hashgraphEvent.selfParentHash].hasTargetAsAncestor) {
            // console.log('Self parent member already visited. Now updating.')
            isVisitedMemberTable[hashgraphEvent.creatorUID] = true
            updateChildEventAlongPathsFromSourceEvent(hashgraph, eventPathTable, hashgraphEvent.uid, isVisitedMemberTable, extract)
          }
        }
      }

      eventQueue.unshift(hashgraphEvent.selfParentHash)
    }

    // Set the other parent event to have the same member list as the current event. Also check if the other
    // parent event is known to have the target as an ancestor to ensure the current event's member list is
    // recorded/visited.
    if (hashgraphEvent.otherParentHash) {
      if (!eventPathTable[hashgraphEvent.otherParentHash]) {
        eventPathTable[hashgraphEvent.otherParentHash] = { hasTargetAsAncestor: false, isAncestorRelationKnown: false, childEventList: [] }
      }
      eventPathTable[hashgraphEvent.otherParentHash].childEventList.push(hashgraphEvent.uid)

      // Similar reasoning is applied here as to the self parent condition.
      if (isVisitedEventTable[hashgraphEvent.otherParentHash]) {
        if (eventPathTable[hashgraphEvent.otherParentHash].isAncestorRelationKnown) {
          if (eventPathTable[hashgraphEvent.otherParentHash].hasTargetAsAncestor) {
            isVisitedMemberTable[hashgraphEvent.creatorUID] = true
            updateChildEventAlongPathsFromSourceEvent(hashgraph, eventPathTable, hashgraphEvent.uid, isVisitedMemberTable, extract)
          }
        }
      }

      eventQueue.unshift(hashgraphEvent.otherParentHash)
    }
  }

  let numVisitedMembers = 0

  for (const memberUID of memberUIDList) {
    numVisitedMembers += isVisitedMemberTable[memberUID] ? 1 : 0
  }

  const hasVisitedSupermajority = numVisitedMembers >= Math.ceil((2 / 3) * memberUIDList.length)
  return hasVisitedSupermajority
}

function updateChildEventAlongPathsFromSourceEvent (
  hashgraph: Hashgraph,
  eventPathTable: EventPathTable,
  sourceEventUID: string,
  isVisitedMemberTable: { [memberUID: string]: boolean },
  _extract?: any) {
  const childEventList = eventPathTable[sourceEventUID].childEventList

  const childEventQueue: string[] = []
  const isVisitedChildEventTable: { [eventUID: string]: boolean } = {}
  for (const childEventUID of childEventList) {
    let eventUID: string = childEventUID

    childEventQueue.unshift(eventUID)

    while (childEventQueue.length > 0) {
      eventUID = childEventQueue.pop() || ''
      if (!eventUID || isVisitedChildEventTable[eventUID]) {
        continue
      }

      isVisitedChildEventTable[eventUID] = true

      if (!eventPathTable[eventUID]) {
        eventPathTable[eventUID] = { hasTargetAsAncestor: false, isAncestorRelationKnown: false, childEventList: [] }
      }

      if (eventPathTable[eventUID].isAncestorRelationKnown) {
        continue
      }

      eventPathTable[eventUID].hasTargetAsAncestor = true
      eventPathTable[eventUID].isAncestorRelationKnown = true

      const hashgraphEvent = hashgraph.eventTable[eventUID]

      if (!hashgraphEvent) { continue }

      isVisitedMemberTable[hashgraphEvent.creatorUID] = true

      const nextChildEventList = eventPathTable[eventUID].childEventList

      for (const nextChildEventUID of nextChildEventList) {
        childEventQueue.unshift(nextChildEventUID)
      }
    }
  }
}

export {
  canSeeEvent,
  canSeeEventThroughMany
}